REAL FUNCTION f(x)
	REAL :: x
	f = x**2
END FUNCTION

PROGRAM trapeze
	INTEGER count
	REAL xp, xk, f0, f1, dx, x0, x1, sum

	count = 10
	xp = -1.0
	xk = 1.0

	dx = (xk - xp) / count
	x0 = xp
	x1 = xp + dx
	sum = 0
	DO i = 1, count
		f0 = f(x0)
		f1 = f(x1)
		sum = sum + (f0 + f1) * dx / 2.0
		x0 = x1
		x1 = x1 + dx
	END DO
	WRITE(*, *) sum
END PROGRAM trapeze
